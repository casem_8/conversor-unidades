﻿using ConversionUnidades.Clases;
using System.Globalization;

// Establecer el punto como separador decimal
CultureInfo culturaUS = new CultureInfo("en-US");
CultureInfo.CurrentCulture = culturaUS;

while (true)
{
    // Menu
    Console.WriteLine("***********************************");
    Console.WriteLine("Bienvenido al conversor de unidades");
    Console.WriteLine("Seleccione el tipo de conversión:");
    Console.WriteLine("1. Longitud");
    Console.WriteLine("2. Masa");
    Console.WriteLine("3. Temperatura");
    Console.WriteLine("4. Salir");

    string valorEntrada = Console.ReadLine();

    switch (valorEntrada)
    {
        case "1":
            RealizarConversionDeLongitud();
            break;
        case "2":
            RealizarConversionDeMasa();
            break;
        case "3":
            RealizarConversionDeTemperatura();
            break;
        case "4":
            return; // Salir de la aplicación
        default:
            Console.WriteLine("Opción no válida. Por favor, seleccione una opción válida.");
            break;
    }
}

// Conversión para longitud
static void RealizarConversionDeLongitud()
{
    var conversorLongitud = new ConversorLongitud();

    Console.WriteLine("Seleccione la conversión de longitud:");
    Console.WriteLine("1. Metros a Kilómetros");
    Console.WriteLine("2. Centímetros a Pulgadas");
    Console.WriteLine("3. Kilómetros a centimetros");

    string tipoConversion = Console.ReadLine();

    double valor;

    switch (tipoConversion)
    {
        case "1":
            Console.Write("Ingrese la longitud en metros: ");
            try
            {
                valor = double.Parse(Console.ReadLine());
                double resultadoKilometros = conversorLongitud.ConvertirMetrosAKilometros(valor);
                Console.WriteLine($"Resultado: {valor} metros = {resultadoKilometros} kilómetros");
            }
            catch (FormatException)
            {
                Console.WriteLine("Entrada no válida. Por favor, ingrese un número válido.");
            }
            break;

        case "2":
            Console.Write("Ingrese la longitud en centímetros: ");
            try
            {
                valor = double.Parse(Console.ReadLine());
                double resultadoPulgadas = conversorLongitud.ConvertirCentimetrosAPulgadas(valor);
                Console.WriteLine($"Resultado: {valor} centímetros = {resultadoPulgadas} pulgadas");
            }
            catch (FormatException)
            {
                Console.WriteLine("Entrada no válida. Por favor, ingrese un número válido.");
            }
            break;

        case "3":
            Console.Write("Ingrese la longitud en kilómetros: ");
            try
            {
                valor = double.Parse(Console.ReadLine());
                double resultadoCentimetros = conversorLongitud.ConvertirKilometrosACentimetros(valor);
                Console.WriteLine($"Resultado: {valor} Kilómetros = {resultadoCentimetros} centímetros");
            }
            catch (FormatException)
            {
                Console.WriteLine("Entrada no válida. Por favor, ingrese un número válido.");
            }
            break;

        default:
            Console.WriteLine("La opción que seleccionó no es válida. Por favor, seleccione una opción válida.");
            break;
    }
}

// Conversión para masa
static void RealizarConversionDeMasa()
{
    var conversorMasa = new ConversorMasa();

    Console.WriteLine("Seleccione la conversión de longitud:");
    Console.WriteLine("1. Kilogramos a libras");
    Console.WriteLine("2. Toneladas a kilogramos");
    Console.WriteLine("3. Libras a gramos");

    string tipoConversion = Console.ReadLine();

    double valor;

    switch (tipoConversion)
    {
        case "1":
            Console.Write("Ingrese la masa en kilogramos: ");
            try
            {
                valor = double.Parse(Console.ReadLine());
                double resultadoLibras = conversorMasa.ConvertirKilogramosALibras(valor);
                Console.WriteLine($"Resultado: {valor} kilogramos = {resultadoLibras} libras");
            }
            catch (FormatException)
            {
                Console.WriteLine("Entrada no válida. Por favor, ingrese un número válido.");
            }
            break;

        case "2":
            Console.Write("Ingrese la masa en toneladas: ");
            valor = double.Parse(Console.ReadLine());
            double resultadoKilogramos = conversorMasa.ConvertirToneladaAKilogramos(valor);
            Console.WriteLine($"Resultado: {valor} toneladas = {resultadoKilogramos} Kilogramos");
            break;

        case "3":
            Console.Write("Ingrese la masa en libras: ");
            try
            {
                valor = double.Parse(Console.ReadLine());
                double resultadoGramos = conversorMasa.ConvertirLibrasAGramos(valor);
                Console.WriteLine($"Resultado: {valor} libras = {resultadoGramos} gramos");
            }
            catch (FormatException)
            {
                Console.WriteLine("Entrada no válida. Por favor, ingrese un número válido.");
            }
            break;
        default:
            Console.WriteLine("La opción que seleccionó no es válida. Por favor, seleccione una opción válida.");
            break;
    }
}
// Conversión para temperatura
static void RealizarConversionDeTemperatura()
{
    var conversorTemperatura = new ConversorTemperatura();

    Console.WriteLine("Seleccione la conversión de longitud:");
    Console.WriteLine("1. Celsius a Fahrenheit");
    Console.WriteLine("2. Fahrenheit a Celsius");

    string tipoConversion = Console.ReadLine();

    int valor;

    switch (tipoConversion)
    {
        case "1":
            Console.Write("Ingrese la temperatura en grados Celsius: ");
            try
            {
                valor = int.Parse(Console.ReadLine());
                int resultadoFahrenheit = conversorTemperatura.CelsiusAFahrenheit(valor);
                Console.WriteLine($"Resultado: {valor} °C = {resultadoFahrenheit} °F");
            }
            catch (FormatException)
            {
                Console.WriteLine("Entrada no válida. Por favor, ingrese un número válido.");
            }
            break;

        case "2":
            Console.Write("Ingrese la temperatura en grados Fahrenheit: ");
            try
            {
                valor = int.Parse(Console.ReadLine());
                double resultadoCelsius = conversorTemperatura.FahrenheitACelsius(valor);
                Console.WriteLine($"Resultado: {valor} °F = {resultadoCelsius} °C");
            }
            catch (FormatException)
            {
                Console.WriteLine("Entrada no válida. Por favor, ingrese un número válido.");
            }
            break;
        default:
            Console.WriteLine("La opción que seleccionó no es válida. Por favor, seleccione una opción válida.");
            break;
    }
}