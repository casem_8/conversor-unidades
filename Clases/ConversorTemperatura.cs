﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionUnidades.Clases
{
    public class ConversorTemperatura
    {
        public int CelsiusAFahrenheit(int celsius)
        {
            //return 0; // Falla de la prueba
            int farenheit = celsius * 9 / 5 + 32;
            return farenheit;
        }

        public int FahrenheitACelsius(int fahrenheit)
        {
            //return 0;
            int celsius = (fahrenheit - 32) * 5 / 9;
            return celsius;
        }
    }
}
