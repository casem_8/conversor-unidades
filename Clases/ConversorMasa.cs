﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionUnidades.Clases
{
    public class ConversorMasa
    {
        public double ConvertirKilogramosALibras(double kilogramos)
        {
            //return 0; // Falla de la prueba
            double libras = kilogramos * 2.20462;
            return Math.Round(libras, 2);
        }

        public double ConvertirToneladaAKilogramos(double toneladas)
        {
            //return 0;
            double kilogramos = toneladas * 1000;
            return Math.Round(kilogramos, 2);
        }

        public double ConvertirLibrasAGramos(double libras)
        {
            //return 0;
            double gramos = libras * 453.59;
            return Math.Round(gramos, 2);
        }
    }
}
