﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionUnidades.Clases
{
    public class ConversorLongitud
    {
        public double ConvertirMetrosAKilometros(double metros)
        {
            //return 0; // Falla de la prueba con red
            double kilometros = metros / 1000; // Pasa la prueba con green
            return Math.Round(kilometros, 2); // Refactor para redondear el resultado
        }

        public double ConvertirCentimetrosAPulgadas(double centimetros)
        {
            //return 0;
            double pulgadas = centimetros / 2.54;
            return Math.Round(pulgadas, 2);
        }

        public double ConvertirKilometrosACentimetros(double kilometros)
        {
            //return 0;
            double centimetros = kilometros * 100000;
            return Math.Round(centimetros, 2);
        }
    }
}
